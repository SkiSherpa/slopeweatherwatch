## 2023-11-16 Battling with Git
I have been trying to push another project, it has not been going well. I have tried to merge new branches, merge histories, and rebase. None has worked.

## 2023-11-9 Solved Image Logo Display
*Ethan helped with the solve, you do have to use the tag as `Image`, AND import it Image from "next/image". It works as a component or inline like how I had it before. I'm going to keep it as a component though.*

### Logo
Currently the logo is blank:
![[Screenshot 2023-11-08 at 2.49.46 PM.png|300]]

The blank rounded square is done by this line:
```JavaScript
<span className="h-7 w-7 bg-zinc-300 rounded-lg" />
```

I replaced that line with:
```JavaScript
<img src={LogoImage} alt="Logo" className="h-7 w-7" />
```
I'm getting a red squiggle under src and image is the broken symbol on browser.

checking to make sure the file is a png in terminal:
```
file src/logoImage/LogoImage.png
```
I am returned:
```
src/logoImage/LogoImage.png: PNG image data, 32 x 32, 8-bit/color RGBA, non-interlaced
```
It is a png file.

I checked my import statement. I'm confident that it is correct.
File structure:
![[Screenshot 2023-11-08 at 4.41.35 PM.png|130]]

Is it possible that the h-7 w-7 is constricting it some how? - no, but it is making the window to display 7x7 pixels. The photo you're using is 32x32 pixels. I changed `className="h-32 w-32"`

I know the import statement is correct. I've tried moving the files around to different folders: src/logoImage/LogoImage.png. Moved it back to favicon. Tried the `favivon-16x16.png`. Deleted .next. From online it seems like I'm doing everything correctly. There is another way of making the image a component, and using it as a component.

###### Component attempt
The docs say the Image tag needs a src, width, height, and alt, with this example:
https://nextjs.org/docs/app/api-reference/components/image
```JavaScript
import Image from 'next/image'

export default function Page() {
  return (
    <div>
      <Image
        src="/profile.png"
        width={500}
        height={500}
        alt="Picture of the author"
      />
    </div>
  )
}
```

My component looks like this:
```javascript
import Logo from "./Logo.png";

export default function Logo() {
    return (
        <>
            <Image src="./Logo.png" width={32} height={32} alt="Website logo" />
        </>
    )
}
```

file structure, just in case:
![[Screenshot 2023-11-08 at 4.41.35 PM.png|130]]

I get a red sqgl under the whole `image`.  `Image` causes the server to throw an error. I switched back to `img` in component file and tried importing inline.
```JavaScript
export default function LogoImage() {
    return (
        <>
            <img src="./Logo.png" width={32} height={32} alt="Website logo" />
        </>
    )
}
```

When I go back to
```javascript
src={Logo}
```
but under `src` red sqgl persists.

**header.tsx**
```javascript
import LogoImage from "@/logoImage/LogoImage";
```
and
```javascript
            <LogoImage />
```

I think this might be a "use client situation".

**SOVLED**
You do have to use image as `Image` and import. So the component code now looks like:
```javascript
import Image from "next/image";
import Logo from "./Logo.png";

export default function LogoImage() {
    return (
        <>
            <Image src={Logo} width={32} height={32} alt="Website logo" />
        </>
    )
}
```

Doing the same thing inline also works.

## 2023-11-8
*Built the header component. Found out that desktop routing has nothing to do with styles/constants.tsx. Read Routing in Next.js. Watched the header-mobile part of video, but did not implement the code. It's much more complicated and I just am trying to learn next.js. Will build the side bar portion next time. Want to try to add the logo to the logo part. start at 14:56*

*I tried to put a logo into logo part in the code Hosna wrote, but haven't gotten it to work. The closest I've gotten is a broken image symbol. I think this might be a "use-client" situation. I will wait and see if the her video will describe how to do it. I'm also seeing Ethan tomorrow and see if he knows.*

start at 6:16 from Hosna's video: https://www.youtube.com/watch?v=3pTPcyUJx-Q&ab_channel=HosnaQasmei

### Routing in Next.js:
It seems like all you need for desktop is having a folder be a sibling of layout, probably in the app folder, our router, and naming the file `page.tsx`

start at 6:16 from Hosna's video: https://www.youtube.com/watch?v=3pTPcyUJx-Q&ab_channel=HosnaQasmei
### Header
in components > header
adding `use client` and 4 imports:
```Javascript
import Link from "next/link";
import { useSelectedLayoutSegment } from 'next/navigation';

import useScroll from '@/hooks/use-scoll';
import { cn } from "@/lib/utils";
```

`useSelectedLayoutSegment` returns a string of the active segment or `null` if one doesn't exist. - Used for navigation, next.js docs

**useScroll** - track how far down a user has scrolled down on a webpage. This can help track which tab or button a user is on and animate that tab/button when they are over depending on how far they have scrolled.

add in some constants:
```JavaScript
const scolled = useScroll(5);
const selectedLayout = useSelectedLayoutSegment();
```

6:57 - add more to the return to make header:
```JavaScript
return (
        <div
      className={cn(
        // sticky >> to stick to the top of page
        // inset-x-0 >> postioning element, makes header stretch from most left & right side of the page
        // z-30, pulling it to the front, as long as nothing gets higher than 30 in the app
        // boarder-b >> border on the bottom
        // w-full >> goes full width of the container
        `sticky inset-x-0 top-0 z-30 w-full transition-all border-b border-gray-200`,
        {
            // when scrolled = true, do these properties
            // when selectedLayout = true, do these properties
          'border-b border-gray-200 bg-white/75 backdrop-blur-lg': scrolled,
          'border-b border-gray-200 bg-white': selectedLayout,
        },
      )}
    >
      <div className="flex h-[47px] items-center justify-between px-4">
        <div className="flex items-center space-x-4">

          <Link
            href="/"
            className="flex flex-row space-x-3 items-center justify-center md:hidden"
          >
            <span className="h-7 w-7 bg-zinc-300 rounded-lg" />
            <span className="font-bold text-xl flex ">Slope Weather Watch</span>
          </Link>
        </div>

        <div className="hidden md:block">
          <div className="h-8 w-8 rounded-full bg-zinc-300 flex items-center justify-center text-center">
            <span className="font-semibold text-sm">HQ</span>
          </div>
        </div>
      </div>
    </div>
    );
```

First `Link` is the logo
The 2nd div, `<div className="hidden md:block">`
shows the logo when the window is small, hides it when screen is wide.

8:05 - header mobile
I feel like there's already a lot here. I will skip this part in my code. Will still watch this part.

Will delete the HeaderMobile Component from layout.

She also has styles folder as a sibling of app. Currently my styles folder is in app. Everything still seems to work though.

The path in styles/constants.tsx seems to be only related to routing for mobile. I don't think that routing for the desktop is dependent on this file. I did change the path names yesterday and it had no effect on the summit page. Routing only seems to be taken care of by the name of the folder and naming that page `page.tsx`

I've commented out the constants side-nav code, and the routes still work.

14:56 - start the side-nav.

### Logo
Currently the logo is blank:
![[Screenshot 2023-11-08 at 2.49.46 PM.png|300]]

The blank rounded square is done by this line:
```JavaScript
<span className="h-7 w-7 bg-zinc-300 rounded-lg" />
```

I replaced that line with:
```JavaScript
<img src={LogoImage} alt="Logo" className="h-7 w-7" />
```
I'm getting a red squiggle under src and image is the broken symbol on browser.

checking to make sure the file is a png in terminal:
```
file src/logoImage/LogoImage.png
```
I am returned:
```
src/logoImage/LogoImage.png: PNG image data, 32 x 32, 8-bit/color RGBA, non-interlaced
```
It is a png file.

I checked my import statement. I'm confident that it is correct.

Is it possible that the h-7 w-7 is constricting it some how? - no, but it is making the window to display 7x7 pixels. The photo you're using is 32x32 pixels. I changed `className="h-32 w-32"`

I know the import statement is correct. I've tried moving the files around to different folders: src/logoImage/LogoImage.png. Moved it back to favicon. Tried the `favivon-16x16.png`. Deleted .next. From online it seems like I'm doing everything correctly. There is another way of making the image a component, and using it as a component.

The docs say the Image tag needs a src, width, height, and alt, with this example:
```JavaScript
import Image from 'next/image'

export default function Page() {
  return (
    <div>
      <Image
        src="/profile.png"
        width={500}
        height={500}
        alt="Picture of the author"
      />
    </div>
  )
}
```

My component looks like this:
```javascript
import Logo from "./Logo.png";

export default function Logo() {
    return (
        <>
            <Image src="./Logo.png" width={32} height={32} alt="Website logo" />
        </>
    )
}
```

file structure
## 2023-11-7
*started on the nav bar, in video he does a header nav. I want a header in general for the title and side nav bar, plus I want routes. I found another video that does a side nav bar and the lady goes over routes. A Draw back is that there is not a lot of explanation.*

*We installed some packages and set up some new folders with new files. The folders are siblings to app folder, see photo below for new files. Details as to what is in the files is below. I set up a route for the home page and for snow summit as a page. I found for the routes to work the file for snowSummit has to be `page.tsx` and placed in the snowSummit folder. Also changing the name of the folder is bad, hit some errors, but was easily fixed. It's just hard to change the file name after its been made and imported so many places (I think just 1?). The utils and use-Scroll file was copied and pasted, was done at the end of the day as it popped up in the header code which I will tackle tomorrow. Hosna said it was standard, the other video the guy does have a lib and components folder, no hooks, but I think we made our own hook. I'm cool with taking it at face value.*

*The header files, one for desktop and one for mobile were added to layout above children as they are headers. Both appear as they should in both home and summit pages. We will start tomorrow at 6:16 in Hosna's video.*
1:06:23 - nav bar
He does the header for the nav bar. I would like it on the left side. Will start research.

tailwind docs on side nav bar:
https://tw-elements.com/docs/standard/navigation/sidenav/

installed `tw-elements` library

I copied the JS and HTML code from the docs. I'm very confused by the docs will look for a youtube video. DELETED the nav folder that I copied from the tailwind docs.

#### Side bar
video for side bar: https://www.youtube.com/watch?v=3pTPcyUJx-Q&ab_channel=HosnaQasmei
Goes over making a header, side bar, and routes for desktop and mobile. It might be a little fast once past header, but I think it will be good to try.

installed `tailwind-merge`: for controlling css styles
installed `@iconify/react`: for icons

##### Set up for Routes
created a styles folder in app folder. Created a constants.tsx and type.tsx in styles folder.

**This set up in type and constants is only used for mobile routing.**
type.tsx:
```Javascript
export type SiseNavItem = {
	title: string;
	path: string;
	icon?: JSX.Element;
}
```
? means its optional.
Video has more, but I'm not using sub menus

constants.tsx:
```JavaScript
import { Icon } from "@iconify/react";
import { SideNavItem } from "./types";

export const SIDENAV_ITEMS: SideNavItem[] = [
	{
		title: "Home",
		path: "/",
		icon: <Icon icon="lucide:home" width="24" height="24" />,
	},
	{
		title: "SnowSummit",
		path: "/SnowSummit",
		icon: <Icon icon="lucide:home" width="24" height="24" />,
	}
]
```

at 2:45 she adds to a lib folder and adds a `utils.tsx` file for styling and the `tailwind-merge` comes into play. Also added a hooks folder and use-scroll.tsx, she say that its pretty standard. (I'm going to to do the routes part first)

**This is used in desktop:**
added:
	lib (folder) > utils.tsx
	hooks (folder) > use-scroll.tsx

need to install clsx.
**utils** file seems to be standard, just taking the lady's word:
```JavaScript
import { clsx, type ClassValue } from "clsx";
import { twMerge } from "tailwind-merge";
export const cn = (...inputs: ClassValue[]) => {
return twMerge(clsx(inputs));
}
```
needs to be const and not function, otherwise red squiggles

**useScroll:**
I think were making a hook:
```JavaScript
import { useCallback, useEffect, useState } from 'react';
export default function useScroll(threshold: number) {
  const [scrolled, setScrolled] = useState(false);

  const onScroll = useCallback(() => {
    setScrolled(window.scrollY > threshold);
  }, [threshold]);

  useEffect(() => {
    window.addEventListener('scroll', onScroll);
    return () => window.removeEventListener('scroll', onScroll);
  }, [onScroll]);

  // also check on first load
  useEffect(() => {
    onScroll();
  }, [onScroll]);

  return scrolled;
}
```
checking for how far down a user a scrolled down on a page and returning a bool

##### Routes
For Routing files have to be called `page.tsx`. The routing for the snowSummit is snowSummit (folder) > page.tsx

To test if the folder name is in charge of the routing in the url I changed the file name from snowSummit to SnowSummit. Then I got internal sever errors on the summit page as well as home.

After the error, routes section is finished
###### Error: internal server error - SOLVED
Nothing would load. Renamed it back to snowSummit, deleted .next file and restarted server. Everything fixed.

##### Header
in the scr folder created a `components` folder. Sibling to app folder. In components created a `header.tsx` and `header-mobile.tsx` file.

Imported both into `layout.tsx`  and placed above `children`. That how html structure thing.
```JavaScript
return (
	<html lang="en">
		<body suppressHydrationWarning={true} className={`${inter.className} bg-stone-200 text-slate-800`}>
			<Header />
			<HeaderMobile />
			{children}
		</body>
	</html>
)
```
Both appear on each route.
![[Screenshot 2023-11-07 at 2.18.57 PM.png|300]]
![[Screenshot 2023-11-07 at 2.18.44 PM.png|300]]

In component > `header.tsx` at 6:16
## 2023-11-6 Errors, Framer-motion, CSR and SSR
*I wanted to implement the framer-motion, but tried with a button rather than the header used in the video. I ran into validateDOMNesting error, masquerading as a hydration error, but was fixed by placing SnowSummit component in body tag after {children}. I added motion.button with the examples from the docs. Got many context errors. Research context, still confused. The **solution** was to add "use client" to the top of the snowSummit component file. Anything in the app folder is defaulted to a server-side component and does server side rendering (SSR). However, for a page to be interactive it needs to be a client-side rendered (CSR) page. A button is something interactive. Biggest take aways: keeping the html structure, and learning/implementing SSR or CSR files. Video end at 1:02:50*

start at 59:39
Installing a package `npm install framer-motion` - he uses it for scrolling, but it has animations for hover and tap gestures which could be useful. I installed.
https://www.framer.com/motion/introduction/
`import {motion} from "framer-motion"`
Then always something like, you have a tag, `<motion.div>`. You place the keyword motion infront of the tag. If you have a button to animate or place a hover over it would be
```JavaScript
<motion.button
	whileHover={{
	scale.1.2,
	transition: {duration: 1},
	}}
```

###### Error: Solved - hydration, validateDOMNEsting
I keep running into the hydration error. Going to turn off the following Chrome extensions:
- [ ] huntr
- [ ] loom
- [ ] honey
- [ ] simplify
- [ ] grammerly

I also have a `Warning: validateDOMNesting(...): <h1> cannot appear as a child of <html>.`

SOLVED: This ^^ is the clue for the fix. I had the return code in layout like this:
```JavaScript
return (

<html lang="en">
	<SummitTest />
	<body suppressHydrationWarning={true} className={`${inter.className} bg-stone-200 text-slate-800`}>
	{children}
	</body>
</html>
)
```

standard html structure is:
```html
<html>
	<head></head>
	<body></body>
</html>
```
In Next, the head info is taken care of by the `export const metadata` line, currently 8. Should be near the top in layout.tsx

The SummitTest component was outside the body, by moving it inside of body, like:
```JavaScript
<body suppressHydrationWarning={true} className={`${inter.className} bg-stone-200 text-slate-800`}>
	{children}
	<SummitTest />
	</body>
```
fixes Hydration and the other error. I turned back on all my extensions did **not** add in the <head></head>

###### Continuing with button test in SummitTest Component
Trying to add a button with framer-motion into SummitTest
###### Error: Solved - made snowSummit into a "client component"
```
node_modules/framer-motion/dist/es/context/MotionConfigContext.mjs (6:42) @ eval
 ⨯ TypeError: (0 , react__WEBPACK_IMPORTED_MODULE_0__.createContext) is not a function
```

Tried deleting .next folder and restarting server -> still the same error.

It has something to do with **React Context**?
**Context:** The small research we did on context
Passing down props down to any children with out writing `passin={passing}` in every component
I'm very confused by context. I'm not sure what the problem is that is fixes or what the fix is. I'm going to continue with video...

**Solved** By adding `"use client";` at the top of the file, it fixes everything. It makes that file a client side rendering (CSR). The bones of the page is rendered on the server, then given to the browser, the browser maintains the state of the page.

Opposed to Server-side Rendering (SSR), everything rendered on the server and given to the browser.

###### CSR & SSR
**Client-Side Rendering (CSR)**: The bones of the page is rendered on the server, then given to the browser, the browser maintains the state of the page.
	Pros:
		 - Interactivity and User exp - smooth interactions with out requiring the page to reload
		 - initial load times. Moving between pages, client side JS has been loaded and can nav without needing a request new HTML
		 - Reduce Server load
		 - Easier to debug, you have logs from clients and server

**Server-Side Rendering (SSR)**: Everything is rendered on the server and sent to the browser
	Pros:
		- Better for search engines
		- Security - authentication flows, reducing of exposing info.
		- Reduce Client Side load - browser on has so much mem





end at 1:02:50
## 2023-11-3 Decided DB, changing background/text color, import statement alias @, app is a router as well as a folder, hydration error fix.
Summary: *Decided on MongoDB for the db. Changed the background/text colors. Learned about the @ alias for import statement. When testing import found that the file structure is src > app > everything else. Moving files to the src doesn't work as the app folder is a router, this was set up when Next.js asked all those questions at the beginning. After moving the files back I found I had an error in the console, getting extra attributes from the server. I thought it had something to do with moving of the files, app router folder, and next.config, BUT entirely possible that it was there before as I didn't have the console open. It's a hydration issue where the my local codes attributes mismatch with the client side attributes. Can be caused by browsers extensions like grammerly. I added an attribute to the body tag to fix the warning. video end at 1:02:22*

start with changing the background at 36:01.

Decided to go with MongoDB because I'm not sure on the exact structure of data tables. The flexibility of possibly needing to add either more rows or adding new documents seems like the right choice at this moment. If I need something more efficient in the future I can migrate to sql db.

##### Changing background and text color
video start at: 36:01.
Changing background color. In `layout.tsx`, working on the body line, this one before making changes:
```TypeScript
<body className={inter.className}>{children}</body>
```
make the className a template literal  for `inter.className` and add a color number from
https://tailwindcss.com/docs/customizing-colors

Remember Tailwind already was installed with the Next.js prompts.

I added background color `bg` and text color `text`. Format for inline styling `{things to color}-{name of color}-{shade/tinting}`.

```TypeScript
<body className={`${inter.className} bg-stone-200 text-slate-800`}>{children}</body>
```

He goes on to make a blurred extra colors on the background. I don't think I want that. Background color and text part ends at ~46:00

47:00 making Lib folder, to import the text for the portfolio page, it might be useful later, but I'm not sure for what.
52:00 start making the header. The header will scroll down to the section that its on, not useful for my site as I want different end points
##### Import statements
57:07 talks about the `@` in front of the paths for some import statements. In `tsconfig.json` there is a "paths" and it defines the `@` to the `root/src/`,  as long as an files or components are in the src folder you can use the `@` in front of the path, it will be the correct path. **`@` is an alias**

Tested out using the `@` alias. It seems to work, but have figured out that I the file structure is src -> app -> everything else.

##### Trying to move the files out of app.
So the **app folder** is not just a folder and is a **Router**. Moving all the files out of there is not the best idea. I moved them back, but have encountered a new error, a
`# Warning: Extra attributes from the server: data-lt-installed`
##### Suppression Error
`# Warning: Extra attributes from the server: data-lt-installed`
May not be related to the moving of the files
`there are extra attributes from the server that are not expected, and it's causing an error in the rendering process.`
It seems to be a Hydration warning, when the tags in your code and what is rendered on the client are different. It can happen with extensions, like grammerly. I'm not sure if that's why its happening for me. Why now and not earlier, although I don't think I had my console up earlier, it could have been there.
To suppress the hydration warning, I added this attribute to the body tag:
`suppressHydrationWarning={true}`

Video is paused at 1:02:22, still in header section.
## 2023-11-1 Learning layout and file structure
*Learning the layout of the given files. Generated a new and replaced the old favicon. Changed the title of the tab.
Video 36:01*
root component of app - src/app layout.tsx
home page - src/app page.tsx

It seems that each page needs to be called `page.tsx`
for {domain}/snowsummit - I made a snowSummit folder and place in another `page.tsx`

in layout the `{children}` in
```TypeScript
return (
	<html lang="en">
		<body className={inter.className}>{children}</body>
	</html>
)
```
is pulling from src/app page.tsx, {children} is page.tsx

to import the
### Deleting stuff in between main tag in root page.tsx
removed import, it contained an image.
##### File structure:
![[Screenshot 2023-11-01 at 3.35.31 PM.png|100]]
The **favicon.ico** controls the little icon in the tab title. If you change. I changed the favicon icon to blue and white S.



## 2023-10-31 Getting started with Next.js
*Summary of today: Started project with `npx create-next-app@ 13.48`. However, when the server is started it says 14.0.1. I'm inclined to believe that is the version. I had to update node on my computer to 20.9.0 to have it be compatible with next.js to get the server to start. Currently at 30:38 in video.*

*It does not seem that their are any easy api's for the specific temperatures that I am looking for. I could do something with a general temp, maybe base and summit for other mountains. *

Starting the video that makes the portfolio page, but will help get started with TypeScript, Next.js, and Vercel for deployment.

Using **Next.js**, the way the vid started was a blank dir, it would not work with a readme in the file. I ran this:
`npx create-next-app@13.4.8 .`
The `.` was to not make a new folder and to just make all the files in the current dir

I got prompted to respond to a few questions:
![[Screenshot 2023-10-31 at 2.50.35 PM.png]]
The video said `no` to src dir, but I said yes as I expect to have a few endpoints and it seems like good practice.

**You can rewind on the video with the arrow keys. It will skip by a few seconds.**

You can start up the server right away:
`npm run dev`

^^ at 30:01.
I hit an error from the `npm run dev` line:
`ReferenceError: Request is not defined`

The issue was that the Next.j version of 13.4.8 is not compatible with my version of Node.js 16.17.0. I updated `n` (not exactly sure, but it seems to control Node) and update to Node.js 20.9.0. Ran the `npm run dev` and the page loads. With a lot of stuff:![[Screenshot 2023-10-31 at 3.10.52 PM.png]]
Currently at 30:38.

## **2023-10-30** DB stuff
I've out lined how to organize the data for each mountain:
{mtn} -> {date} -> {table of each day}
An example:

| hour of the day | station 1 | station 2 | ..... |date|mtn|
|-----------|---|---|---|---|---|
|0000|20|19|...|2023-12-27|Snow Summit|
|0100|19|18|...|2023-12-27|Snow Summit|
|0200|18|18|...|2023-12-27|Snow Summit|
|...|...|...|...|2023-12-27|Snow Summit|
|2300|21|21|...|2023-12-27|Snow Summit|

I came up with new routing URL's and want to run the idea by Cheng.

I'm still torn between which DB to choose. MongoDB would give me flexibility moving forward. Its also a good choice for nested data, which is how I have things currently set up. The argument for a postgreSQL would be to just have tables for a mountain like the one above.

## **2023-10-26** **Met with Cheng discussed plan for app**
We discussed the tech stack. He brought up some questions about which type of DB to use, and I need to do some research moving forward. He brought up Routing, which I didn't consider, but now am thinking about. Presented wire frame. We also discussed how to get the data. Will look into other ways to get the data apart from snow summits site. If snow summit's site is the way to go, I will need to learn how to "scrape" a site, as all the numbers are in the html.

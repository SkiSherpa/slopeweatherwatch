"use client";

import React from 'react';
import Link from "next/link";
import { useSelectedLayoutSegment } from 'next/navigation';
import useScroll from '@/hooks/use-scoll';
import { cn } from "@/lib/utils";
import LogoImage from "@/logoImage/LogoImage";


export default function Header() {
    // not sure why set to 5
    const scrolled = useScroll(5);
    const selectedLayout = useSelectedLayoutSegment();

    return (
        <div
      className={cn(
        // sticky >> to stick to the top of page
        // inset-x-0 >> postioning element, makes header stretch from most left & right side of the page
        // z-30, pulling it to the front, as long as nothing gets higher than 30 in the app
        // boarder-b >> border on the bottom
        // w-full >> goes full width of the container
        `sticky inset-x-0 top-0 z-30 w-full transition-all border-b border-gray-200`,
        {
            // when scrolled = true, do these properties
            // when selectedLayout = true, do these properties
          'border-b border-gray-200 bg-white/75 backdrop-blur-lg': scrolled,
          'border-b border-gray-200 bg-white': selectedLayout,
        },
      )}
    >
      <div className="flex h-[47px] items-center justify-between px-4">
        <div className="flex items-center space-x-4">

          <Link
            href="/"
            className="flex flex-row space-x-3 items-center justify-center md:hidden"
          >

            <LogoImage />
            <span className="font-bold text-xl flex ">Slope Weather Watch</span>
          </Link>
        </div>

        <div className="h-8 w-24 hidden md:block">
          <div className="h-8 w-12 rounded-full bg-zinc-300 flex items-center justify-center text-center">
            <span className="font-semibold text-sm flex-shrink-0">Slope Weather Watch</span>
          </div>
        </div>
      </div>
    </div>
    );
}

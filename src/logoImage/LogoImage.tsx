import Image from "next/image";
import Logo from "./Logo.png";

// to use Image tag you have to import it
// to use as as component it must have a
// src, width, height, alt property
export default function LogoImage() {
    return (
        <>
            <Image src={Logo} width={32} height={32} alt="Website logo" />
        </>
    )
}

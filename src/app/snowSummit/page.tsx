"use client";
// ^^ make this file a "client component" not a server component
// everything in the app folder is a "server component"
import React from "react"
import { motion } from "framer-motion"

export default function SummitTest() {

    function button() {
        console.log('You clisked the button');
    }

    return (

            <>
            <h2>Summit Test</h2>
            <motion.button
                initial={{ opacity: 1 }}
                whileHover={{
                    scale: 1.2,
                    transition: { duration: 1 }
                }}
                onClick={() => button()}
            >I'm a button</motion.button>
            </>

    )
}

import './globals.css'
import { Inter } from 'next/font/google'
import Header from '@/components/header';
import HeaderMobile from '@/components/header-moblie';
// import SummitTest from '@/app/snowSummit/page'

// to use the Inter font. Have to import, instantiat below, and use in className in body tag.
const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  // This controls the tabs title
  title: 'Slope Weather Watch',
  // controls the description that would pop up on google
  description: 'A website to track specific temperature on ski runs to determine snow quality.',
}

export default function RootLayout({
  children,
  } : {
    children: React.ReactNode
  }) {
    return (
      <html lang="en">
        <body suppressHydrationWarning={true} className={`${inter.className} bg-stone-200 text-slate-800`}>
          <Header />
          {children}
        </body>
      </html>
    )
}

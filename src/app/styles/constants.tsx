// navigation item we will have
import { Icon } from "@iconify/react";
import { SideNavItem } from "./types";
// This is used for either very small window, and mainly mobile, which I'm ignoring for now

export const SIDENAV_ITEMS: SideNavItem[] = [
    {
        title: "Home",
        path: "/",
        icon: <Icon icon="lucide:home" width="24" height="24" />,
    },
    {
        title: "SnowSummit",
        path: "/snowSummit",
        icon: <Icon icon="lucide:home" width="24" height="24" />,
    }
]

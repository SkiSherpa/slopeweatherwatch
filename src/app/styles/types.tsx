// This is used for either very small window, and mainly mobile, which I'm ignoring for now
export type SideNavItem = {
    title: string;
    path: string;
    icon?: JSX.Element;
}

// ? means it optional, so title and path are required
